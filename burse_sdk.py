import requests
import json
import pandas as pd

def get_share_price_graph_df(share_id):
    graph_df = requests.get(f'http://127.0.0.1:8000/get_share_price_graph?share_id={share_id}').json()['price_per_share']
    return pd.DataFrame({'sale_date':list(graph_df.keys()),'price_per_share':list(graph_df.values())})

def get_trader_info(trader_id):
    trader_info = requests.get(f'http://127.0.0.1:8000/get_trader_info?trader_id={trader_id}').json()
    return trader_info

def get_all_shares_info():
    all_shares_info = requests.get('http://127.0.0.1:8000/all_share_details').json()
    return all_shares_info

def delete_offer(trader_id,share_id):
    all_shares_info = requests.delete(f'http://127.0.0.1:8000/delete_order?trader_id={trader_id}&share_id={share_id}')

def make_order(trader_id,share_id,amount,price_per_share,is_sell):
    all_shares_info = requests.post(f'http://127.0.0.1:8000/place_order?trader_id={trader_id}&share_id={share_id}&amount={amount}&price_per_share={price_per_share}&is_sell={is_sell}')