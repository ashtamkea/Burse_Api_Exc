import burse_sdk as bs
import pandas as pd

class trader_ai:
    def __init__(self,trader_id):
        self.trader_id = int(trader_id)
        trader_data = bs.get_trader_info(self.trader_id)
        self.money = float(trader_data['money'])
        self.holdings = pd.DataFrame.from_dict(trader_data['holdings'])

    #Calcs the current price_per_share growth percent compares to the last 10 values
    def calc_share_appeal(share_id):
        price_graph = bs.get_share_price_graph_df(share_id).sort_values(by='sale_date',ascending=False).head(10)
        appeal = (price_graph['price_per_share'].iloc[0] - price_graph['price_per_share'].mean()) / price_graph['price_per_share'].mean() * 100
        #capps the value to 100 max in abs terms
        if appeal > 100:
            appeal =  100
        if appeal < -100:
            appeal =  -100
        return appeal

    def sell_share(self,share_id,share_appeal,price_per_share):
        holdings_df = self.holdings[self.holdings['share_id'] == share_id]
        if(holdings_df.shape[0] > 0):
            cur_holdings = holdings_df.iloc[0]['holdings']
            #makes an sell offer based on the current share appeal, grows more desperate as the appeal lowers
            shares_sell_amount = int(cur_holdings * (abs(share_appeal) / 100))
            print(self.trader_id,share_id,shares_sell_amount,price_per_share * ((10 + abs(share_appeal)) / 100),True)
            if(shares_sell_amount > 0):
                bs.make_order(self.trader_id,share_id,shares_sell_amount,price_per_share * ((10 + abs(share_appeal)) / 100),True)

    def buy_share(self,share_id,share_appeal,price_per_share):
        #makes an buy offer based on the current share appeal, grows more desperate as the appeal goes to the moon
        shares_buy_amount = int((self.money / price_per_share) * (share_appeal / 1000))
        if(shares_buy_amount > 0):
            print(self.trader_id,share_id,shares_buy_amount,price_per_share * ((60 + share_appeal) / 100),False)
            bs.make_order(self.trader_id,share_id,shares_buy_amount,price_per_share * ((60 + share_appeal) / 100),False)

    def opportunity_scan(self):
        all_shares_info = bs.get_all_shares_info()
        for share_info in all_shares_info:
            share_id = share_info['share_id']
            share_appeal = trader_ai.calc_share_appeal(share_id)
            bs.delete_offer(self.trader_id,share_id)
            print(share_appeal)
            if(abs(share_appeal) > 5):
                if (share_appeal < 0):
                    print(share_appeal)
                    if(self.holdings.shape[0] > 0):
                        print('got here2')
                        print(self.holdings['share_id'],share_id)
                        if(int(share_id) in self.holdings['share_id']):
                            print('BRO')
                            self.sell_share(int(share_id),share_appeal,share_info['cur_price'])
                else:
                    self.buy_share(share_id,share_appeal,share_info['cur_price'])






