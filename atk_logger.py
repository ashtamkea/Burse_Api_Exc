from ast import arg
import pandas as pd
import time
import platform
from datetime import datetime
from os.path import exists

#Ashtamkea's Logger Lib
log_path = 'Logs.csv'

#Set The Log Path to a new one
def set_path(cur_log_path):
    global log_path
    log_path = cur_log_path

#simple log to be used as a decoration
def log(func):
    def wrapper(*args, **kwargs):
        t0 = time.time()
        val = func(*args, **kwargs)
        time_taken = time.time() - t0

        #RunDetails
        rd = init_rd()
        rd['fun_name'] = func.__name__
        rd['args'] = ','.join(str(arg) for arg in args)
        rd['time_taken'] = time_taken
        write_rd(rd)

        #Return Function Val
        return val
    
    return wrapper

#manual logger to be used when cant use as decorator
def manualog(fun_name = '*', args = '*', time_taken = -1):
    #RunDetails
    rd = init_rd()
    rd['fun_name'] = fun_name
    rd['args'] = args
    rd['time_taken'] = time_taken
    write_rd(rd)

#write run dict to csv log file
def write_rd(rd):
    logdf = pd.DataFrame(columns=[*rd.keys()])
    logdf.loc[logdf.shape[0]] = [*rd.values()]
    add_header = not exists(log_path)
    logdf.to_csv(log_path, mode='a', index=False, header=add_header)

#Initialize the run data dict
def init_rd():
    rd = {}
    rd['run_time'] = str(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    rd['processor'] = platform.processor()
    rd['platform'] = platform.platform()
    rd['node'] = platform.node()
    return rd

