A) Okay, so first of all a little bit about my code in general and the direction I took,
1. I choose to use python and the FASTAPI library for fast development.
2. To speed up processes, I used dataframes to simulate databases. In a real project I would use a SQL database.
3. Was about to finish up the AI, it works but needs some more tweaking as the sell logic doesnt work too well.
4. I assume that I get valid trader_ids and share_ids. Plus, not negative amounts and price_per_share.
5. To activate, install the libraries (Uvicorn, fastapi, pandas, pickle), and run "Uvicorn main:app". Swagger is available at /docs.

B) To store the data, I used just two additional databases, 'orders' and 'transactions'.
1. orders is used to store the current active orders, as expected.
2. the transactions db is used to store all transactions data, and money and share ownership is derived from this db as well. Just like how cryptocurrency transaction logic works.

C) To Initialize the data, used the 'pre_process' notebook. Created the tables, plus filled them in with:
1. The "house" sell orders (initial shares), listed in the orders db. The house's ID is represented as 0.
2. the initial trader's money, and the shares of the house, are listed in the transactions db. Required, since ownership data is derived from the db.
3. the transactions database is divided by a boolean value to real and 'fake' transactions. Fake transactions are like mentioned above (2.), which are not real transactions between two traders (or the house), but just transactions placed to initialize or order the data.
4. fake transactions are also used for the 'random-reaccuring-shareprice-updating', due to the current share price being taken as the price_per_share in the last transaction registered in the Db. In a process that includes both 'random' (fake) and trader transactions (real).

D) Logs are saved in a CSV file inside the data folder.

E) Created the pd_data library, which has classes for orders, transactions, trader, share. Plus useful methods for them.

F) the orders logic works like this:
1. If I placed an order for share 1 at 230 per share, it would go over the best available current sell orders, sorted by cheapest. It would then start buying from them one by one until the amount specified in the order is satisfied.
2. if for example the first matching sell order offers 1000 shares, and I only want to buy 200, the matching order would be updated to 800, and a transaction of 200 between me and the seller would occur.
3. if my order is fully satisfied by existing orders, it would not register, and shares would be bought off existing orders. If my order is not satisfied by the end of the process, it will be recorded in the orders database.
4. the same logic works for sell orders, i only differentiated them by an boolean is_sell var. as the logic behind them both is fairly similar. Stored in the same database and they both are placed by the same request.
5. Before placing orders, traders are first checked if they have the means to do so. For buy orders, that includes money already in other buy orders, and liquid money. for sell, the current share holdings.

G) The price fluctuation algorithm works this way:
1. a thread that accures every 50 secs, all the shares prices are randomly multiplied by a random number between 0.80 and 1.30. Statistically, share prices will always rise but its fine because overall share prices always rise over time, due to inflation(;
2. a fake transaction is registered with the new price of the share. Which means all processes would use this data to calculate the latest transaction price.

H) the get_share_price_graph request returns the share's "price_per_share" over time and can easily be transformes into a plot. (with pandas just requires to add '.plot()')

I) To have the traders AI working:
1. created an library to access the api with ease (burse_sdk)
2. created the trader_ai library, which has the method opportunity_scan, which does the following:
3. get an grasp of the current trend of each share, by calculating the current price's increase or decrease percentwise relative to the mean of the last 10 prices.
4. If the trend is upwards, tries to buy the share, based on the trader's means. would buy for more then the current price as the trend is higher. To the moon!
5. If the trend is downwards, tries to sell shares if have some in posession. Would sell for less than current price as the downwards trend is higher.
6. The buy logic worked, some problems in the sell logic.
7. opportunity_scan method activated for a random trader every 25 secs by running the "activate_ai" notebook.


