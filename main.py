from fastapi import FastAPI, HTTPException
import atk_logger as al
import pd_data as da
import time
from threading import Thread

#Create API Obj
app = FastAPI()
al.set_path('Data/Logs.csv')
da.Load_dfs()

#Place a order
@app.post('/place_order')
def place_order(trader_id:int, share_id:int, amount:int, price_per_share:float, is_sell:bool):
    trader_id = int(trader_id); share_id = int(share_id); amount = int(amount); price_per_share = float(price_per_share); is_sell = bool(is_sell)
    price = price_per_share * amount
    trader = da.trader(trader_id)
    if(is_sell):
        has_means = trader.get_share_sum(share_id) >= amount #if trader have the means to make the sell order, which means enough shares
        print(trader.get_share_sum(share_id), amount)
    else:
        has_means = trader.get_money_sum() - trader.money_in_orders_sum() >= price #If the trader has enough money to place the buy order, including money already put in active buy orders
    if(has_means):
        transaction_objs = {}
        transaction_objs['transactions'] = []
        order = da.order(trader_id, share_id, amount, price, is_sell) #Creates order obj
        if(not order.is_exist()):
            order_matches = order.find_matches() #auto check for matches
            for index, order_match in order_matches.iterrows():# go over matches
                if(order.amount < 1):#if the order has benn setesfied
                    break
                if(order_match['amount'] > order.amount):#if the sell order fully sedesfies the buy order
                    #Updates matching sell order
                    order_match_obj = da.order(order_match['trader_id'],order_match['share_id'])
                    order_match_obj.subtract_amount(order.amount)#updates the sell orders remaining amount
                    order_match_obj.update()#Rewrites sell order

                    #Creates transaction record
                    transaction_obj = da.transaction(order.trader_id if is_sell else order_match_obj.trader_id, order_match_obj.trader_id if is_sell else order.trader_id , order.share_id, order.amount, order.amount * order_match_obj.price_per_share)
                    transaction_obj.insert()
                    transaction_objs['transactions'].append(transaction_obj)

                    #Update current order
                    order.amount = 0
                else:#if the sell order only partly sadesfies the buy order
                    order_match_obj = da.order(order_match['trader_id'],order_match['share_id'])
                    order.subtract_amount(order_match_obj.amount)#subtract available shares from current order
                    order_match_obj.delete()

                    #Creates transaction record
                    transaction_obj = da.transaction(order.trader_id if is_sell else order_match_obj.trader_id, order_match_obj.trader_id if is_sell else order.trader_id, order.share_id, order_match_obj.amount, order_match_obj.price)
                    transaction_obj.insert()
                    transaction_objs['transactions'].append(transaction_obj)
            if(order.amount > 0):#if buy offer wasn's satesfied by current sell orders
                order.insert()#Inserts the order to the listings
                transaction_objs['order']= order
        else:
            raise HTTPException(status_code=400, detail='Already placed an order on the share')
    else:
        if(is_sell):
            raise HTTPException(status_code=400, detail=f'Not enough available shares to place the sell order ({trader.get_share_sum(share_id)})')
        else:
            raise HTTPException(status_code=400, detail=f'Not enough available money to place the buy order ({trader.get_money_sum() - trader.money_in_orders_sum()})')
    da.Write_dfs()
    al.manualog(fun_name='order',args=f'trader number {trader_id}, made an ' + ('sell' if is_sell else 'buy') + f' order on share number {share_id}. price per share is {price_per_share}, amount {amount}.')#[trader_id, share_id, amount, price, is_sell]
    return transaction_objs

#Delete a order
@app.delete('/delete_order')
def delete_order(trader_id:int, share_id:int):
    trader_id = int(trader_id); share_id = int(share_id)
    order = da.order(trader_id, share_id)
    if(not order.is_exist()):
        raise HTTPException(status_code=400, detail='No order placed')
    order.delete()
    da.Write_dfs()
    al.manualog(fun_name='delete',args=f'trader number {trader_id}, deleted order on share number {share_id}')
    return order

@app.get('/share_details')
def share_details(share_id:int):
    share_id = int(share_id)
    share = da.share(share_id)
    return share

@app.get('/all_share_details')
def all_share_details():
    return da.share.get_all_shares()

@app.get('/get_trader_names')
def get_trader_names():
    return da.trader.get_trader_names()

@app.get('/get_trader_info')
def get_trader_info(trader_id:int):
    return da.trader(trader_id)

@app.get('/get_last_trader_transactions')
def get_last_trader_transactions(trader_id:int):
    return da.transaction.get_last_trader_transactions(trader_id)

@app.get('/get_share_price_graph')
def get_share_price_graph(share_id:int):
    return da.transaction.get_share_price_graph(share_id)

def shuffle_cur_prices():
    da.share.shuffle_cur_prices()
    da.Write_dfs()

def shuffle_cur_prices_activator():
    while 1:
        shuffle_cur_prices()
        time.sleep(50)

shuffle__thread = Thread(target=shuffle_cur_prices_activator).start()