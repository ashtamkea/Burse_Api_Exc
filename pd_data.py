#Imports
import pandas as pd
import pickle as pk
import json
import random

#globvars
dfs = None

#Load DataFrames into memory
def Load_dfs():
    global dfs
    dfs_file = open("Data\DataFrames.pkl", "rb")
    dfs = pk.load(dfs_file)
    dfs_file.close()

#Write DataFrames into memory
def Write_dfs():
    dfs_file = open('Data\DataFrames.pkl', 'wb')
    pk.dump(dfs, dfs_file)
    dfs_file.close()

class order:
    #Due to python's lack of support for function overloading, merged an full info based constactor, with strong-keys based constractor, into one, based on filled vals.
    def __init__(self, trader_id, share_id, amount = None, price = None, is_sell = None):
        self.trader_id = int(trader_id)
        self.share_id = int(share_id)
        if(amount == None and price == None and is_sell == None):
            if(self.is_exist()):
                order_data = order.fetch_data(trader_id, share_id)
                self.amount = int(order_data['amount'])
                self.price = float(order_data['price'])
                self.is_sell = bool(order_data['is_sell'])
                self.price_per_share = float(order_data['price_per_share'])
            else:
                self = None
        else:
            self.amount = int(amount)
            self.price = float(price)
            self.is_sell = bool(is_sell)
            self.price_per_share = self.price / self.amount
    
    #Checks if an object already exists based on trader_id, share_id
    def is_exist(self):
        return dfs['orders'][(dfs['orders'].trader_id == self.trader_id) & (dfs['orders'].share_id == self.share_id)].shape[0] == 1

    #Inserts an offer into the dataframe
    def insert(self):
        dfs['orders'].loc[dfs['orders'].shape[0]] = [self.trader_id,self.share_id,self.amount,self.price,self.price_per_share,self.is_sell]

    #Deletes an offer based on trader_id, share_id
    def delete(self):
        global dfs
        dfs['orders'] = dfs['orders'].drop(dfs['orders'][(dfs['orders'].trader_id == self.trader_id) & (dfs['orders'].share_id == self.share_id)].index)

    #Subtracts amount of shares in an order and accordingly the price
    def subtract_amount(self,amount):
        self.amount = self.amount - amount
        self.price = self.amount * self.price_per_share

    def update(self):
        dfs['orders'].loc[(dfs['orders'].trader_id == self.trader_id) & (dfs['orders'].share_id == self.share_id)] = [self.trader_id,self.share_id,self.amount,self.price,self.price_per_share,self.is_sell]

    #Fetches the trader's data by trader_id
    def fetch_data(trader_id, share_id):
        return dfs['orders'][(dfs['orders'].trader_id == trader_id) & (dfs['orders'].share_id == share_id)].iloc[0]

    #finds matching buy/sell orders, sorted by best offers: if selling, find the highest paying traders, and if buying find the lowest selling traders.
    def find_matches(self):
        matches = dfs['orders'][(dfs['orders'].is_sell != self.is_sell) & (dfs['orders'].share_id == self.share_id)].copy()
        matches['price_per_share'] = matches['price'] / matches['amount']
        if(self.is_sell):
            matches = matches[matches.price_per_share >= self.price_per_share].sort_values(by='price_per_share', ascending=True)
        else:
            matches = matches[matches.price_per_share <= self.price_per_share].sort_values(by='price_per_share', ascending=False)
        return matches

class transaction:
    def __init__(self, seller_id, buyer_id, share_id, amount, price):
        self.seller_id = int(seller_id)
        self.buyer_id = int(buyer_id)
        self.share_id = int(share_id)
        self.amount = int(amount)
        self.price = float(price)
        self.price_per_share = self.price / self.amount
        self.sale_date = pd.Timestamp.now()

    def insert(self):
        dfs['transactions'].loc[dfs['transactions'].shape[0]] = [self.seller_id,self.buyer_id,self.share_id,self.amount,self.price,self.price_per_share,self.sale_date,True]

    def get_last_trader_transactions(trader_id):
        last_trader_transactions =  dfs['transactions'].loc[((dfs['transactions'].seller_id == trader_id) | (dfs['transactions'].buyer_id == trader_id)) & (dfs['transactions'].real == True)].head(8)
        last_trader_transactions['sale_date']  = last_trader_transactions['sale_date'].dt.strftime('%d-%m-%Y %H:%M:%S')
        return json.loads(last_trader_transactions.to_json(orient = "records")) if last_trader_transactions.shape[0] > 0 else None

    def get_share_price_graph(share_id):
        rel_transactions = dfs['transactions'].loc[dfs['transactions'].share_id == share_id]#relevent transactions
        rel_transactions_table = pd.DataFrame(rel_transactions,columns=['sale_date','price_per_share']).set_index('sale_date')
        return rel_transactions_table

class trader:
    def __init__(self, trader_id):
        self.trader_id = int(trader_id)
        if(self.is_exist()):
            trader_data = self.fetch_data()
            self.name = str(trader_data['name'])
            self.money = float(self.get_money_sum())
            open_orders = self.get_open_orders()
            self.open_orders = json.loads(open_orders.to_json(orient = "records")) if open_orders.shape[0] > 0 else None
            holdings = self.get_holdings()
            self.holdings = json.loads(holdings.to_json(orient = "records")) if holdings.shape[0] > 0 else None
        else:
            self = None

    #Checks if an trader exists by trader_id
    def is_exist(self):
        return dfs['traders'][dfs['traders']['id'] == str(self.trader_id)].shape[0] == 1

    #Fetches the trader's data by trader_id
    def fetch_data(self):
        return dfs['traders'][dfs['traders']['id'] == str(self.trader_id)].iloc[0]

    #Retrives the amount of shares of a kind a trader ownes based on his transaction history
    def get_share_sum(self,share_id):
        total_bought = dfs['transactions'][(dfs['transactions']['buyer_id'] == self.trader_id) & (dfs['transactions']['share_id'] == share_id)]['amount'].sum()
        total_sold = dfs['transactions'][(dfs['transactions']['seller_id'] == self.trader_id) & (dfs['transactions']['share_id'] == share_id)]['amount'].sum()
        return total_bought - total_sold

    #Retrives the amount of money based on the traders transaction history
    def get_money_sum(self):
        total_bought = dfs['transactions'][dfs['transactions']['buyer_id'] == self.trader_id]['price'].sum()
        total_sold = dfs['transactions'][dfs['transactions']['seller_id'] == self.trader_id]['price'].sum()
        return total_sold - total_bought
    
    #Returns all of the trader's open orders
    def get_open_orders(self):
        return dfs['orders'][dfs['orders']['trader_id'] == self.trader_id]

    #Summerizes the trader's holdings in a DataFrame
    def get_holdings(self):
        holdings_data = pd.DataFrame({c: pd.Series(dtype=t) for c, t in {'share_id': 'int64', 'share_name': 'object', 'holdings': 'int64'}.items()})
        for index,row in dfs['shares'].iterrows():
            holdings = self.get_share_sum(int(row['id']))
            if(holdings > 0):
                holdings_data.loc[holdings_data.shape[0]] = [int(row['id']),row['name'],holdings]
        return holdings_data

    #Retrives the total amount of money put on buy orders by the trader
    def money_in_orders_sum(self):
        return dfs['orders'][(dfs['orders']['trader_id'] == self.trader_id) & (dfs['orders']['is_sell'] == False)]['price'].sum()

    def get_trader_names():
        return list(dfs['traders']['name'])

class share:
    def __init__(self, share_id):
        self.share_id = int(share_id)
        if(self.is_exist()):
            share_data = self.fetch_data()
            self.name = str(share_data['name'])
            self.cur_price = float(self.get_cur_price())
            open_orders = self.get_open_orders()
            self.open_orders = json.loads(open_orders.to_json(orient = "records")) if open_orders.shape[0] > 0 else None
            last_transactions = self.get_last_transactions()
            last_transactions['sale_date']  = last_transactions['sale_date'].dt.strftime('%d-%m-%Y %H:%M:%S')
            self.last_transactions = json.loads(last_transactions.to_json(orient = "records")) if last_transactions.shape[0] > 0 else None
        else:
            self = None

    def is_exist(self):
        return dfs['shares'][dfs['shares']['id'] == str(self.share_id)].shape[0] == 1
    
    def fetch_data(self):
        return dfs['shares'][dfs['shares']['id'] == str(self.share_id)].iloc[0]
    
    #gets current share price, based on the latest transaction
    def get_cur_price(self):
        last_record =  dfs['transactions'][dfs['transactions']['share_id'] == self.share_id].sort_values(by='sale_date',ascending=False).iloc[0]
        return last_record['price_per_share']

    #returns all open orders on a share
    def get_open_orders(self):
        return dfs['orders'][dfs['orders']['share_id'] == self.share_id]

    #returns the 10 last share transactions
    def get_last_transactions(self):
        return dfs['transactions'][(dfs['transactions']['share_id'] == self.share_id) & (dfs['transactions']['real'] == True)].sort_values(by='sale_date',ascending=False).head(10)
    
    #Retrives all shares, fills in their data, and returns as a list
    def get_all_shares():
        shares_lst = []
        for index,row in dfs['shares'].iterrows():
            shares_lst.append(share(row['id']))
        return shares_lst

    #Creates fake transaction to update share price
    def update_cur_price(self):
        dfs['transactions'].loc[dfs['transactions'].shape[0]] = [0,0,self.share_id,1,self.cur_price,self.cur_price,pd.Timestamp.now(),False]

    #Manipulates Share Prices, a bit up and down, later updates
    def shuffle_cur_prices():
        for index,row in dfs['shares'].iterrows():
            share1 = share(row['id'])
            share1.cur_price = share1.cur_price * ((random.random() / 2) + 0.80)
            share1.update_cur_price()
            

